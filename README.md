# Farm Solution Kenney

MIST 450 Class Project for Grow Ohio Valley

### Prerequisites

This project is built on .NET Core 2.2 as a MVC Web Application

All that is needed is Visual Studio with the C# tools installed


## Built With

* [.NET Core 2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2) - The web framework used


## Authors

* **Tyler Kenney** - [tgkenney](https://github.com/tgkenney)
* **Dr. Nanda Surendra** - *Class Teacher*
